const { Router } = require("express");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const AuthMiddleware = require("../middlewares/AuthMiddleware");

const AuthController = require("../controllers/web/AuthController");
const HomeController = require("../controllers/web/HomeController");

const web = Router();

web.use(bodyParser.json());
web.use(bodyParser.urlencoded({ extended: true }));
web.use(cookieParser());

const authController = new AuthController();
const homeController = new HomeController();

web.get("/login", authController.login);
web.post("/login", authController.doLogin);
web.get("/logout", authController.logout);

// web.use(AuthMiddleware())

web.get("/add", homeController.add);
web.post("/save-user", homeController.saveUser);

web.get("/edit/:id", homeController.edit);
web.post("/save-edit/:id", homeController.saveEdit);

web.get("/delete/:id", homeController.delete);

web.get("/", homeController.index);

module.exports = web;
