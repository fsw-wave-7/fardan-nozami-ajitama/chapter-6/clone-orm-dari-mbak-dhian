'use strict';
const {
  Model
} = require('sequelize');
const { User } = require('./index')

module.exports = (sequelize, DataTypes) => {
  class UserBiodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  UserBiodata.init({
    address: DataTypes.STRING,
    nik: DataTypes.STRING,
    education: DataTypes.STRING,
    userId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'UserBiodata',
  });
  
  UserBiodata.associate = function(models) {
    UserBiodata.belongsTo(models.User, {foreignKey: 'userId', as: 'user'})
  };

  return UserBiodata;
};