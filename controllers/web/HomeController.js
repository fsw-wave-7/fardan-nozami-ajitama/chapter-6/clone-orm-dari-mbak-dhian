const { User, UserBiodata } = require("../../models");
const { join } = require("path");
const bcrypt = require("bcrypt");

class HomeController {
  index = (req, res) => {
    User.findAll().then((users) => {
      res.render(join(__dirname, "../../views/index"), {
        content: "./pages/userList",
        users: users,
      });
    });
  };

  add = (req, res) => {
    res.render(join(__dirname, "../../views/index"), {
      content: "./pages/userAdd",
    });
  };

  saveUser = async (req, res) => {
    const salt = await bcrypt.genSalt(10);

    User.create(
      {
        name: req.body.name,
        username: req.body.username,
        age: req.body.age,
        password: await bcrypt.hash(req.body.password, salt),
        user_biodata: {
          address: req.body.address,
          nik: req.body.nik,
          education: req.body.education,
        },
      },
      {
        include: {
          model: UserBiodata,
          as: "user_biodata",
        },
      }
    )
      .then(() => {
        res.redirect("/");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  edit = (req, res) => {
    User.findOne({
      where: {
        id: req.params.id,
      },
      include: "user_biodata",
    }).then((user) => {
      res.render(join(__dirname, "../../views/index"), {
        content: "./pages/userEdit",
        user: user,
      });
    });
  };

  saveEdit = (req, res) => {
    User.update(
      {
        name: req.body.name,
        username: req.body.username,
        age: req.body.age,
        password: req.body.password,
      },
      {
        where: { id: req.params.id },
      }
    )
      .then((user) => {
        res.redirect("/");
      })
      .catch((err) => {
        res.status(422).json("Can't update user");
      });
  };

  delete = (req, res) => {
    User.destroy({
      where: {
        id: req.params.id,
      },
    }).then(() => {
      res.redirect("/");
    });
  };
}

module.exports = HomeController;
